from django import template

register = template.Library()

@register.filter
def single_url1(text):
    return text.split(",")[0]

@register.filter
def single_url2(text):
    return text.split(",")[1]

@register.filter
def date_get(text,number):
    return text.split("-")[int(number)-1]
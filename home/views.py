import copy
import json
import uuid

from django.conf import settings
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate

from django.contrib.auth.views import logout
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.datetime_safe import datetime, date
import pyrebase
from home.forms import *
import uuid

from django.utils.translation import ugettext as _
# Create your views here.
from home.functions import check_dict_key


def base(req=None):
    data = {
        'now':datetime.now(),
    }
    return data

def check_user(req=None):
    return_val = [0,None]
    try:
        idtoken = req.session['uid']
        a = auth_fb.get_account_info(idtoken)
        print("AAAAAAAAAAAAA  a = {}".format(a))
        a = a['users']
        print("BBBBBBBBBBBBB  a = {}".format(a))
        a = a[0]
        print("CCCCCCCCCCCCC  a = {}".format(a))
        # a = a['localId']
        return_val = [1,a]
    except:
        pass
    return return_val


def base_auth(req=None):
    #
    # try:
    #     req.session['uid']
    # except:
    #     return HttpResponseRedirect(reverse('home:sign_in'))
    data = {
        'now':datetime.now(),
    }
    user = check_user(req=req)
    if user[0] == 1:
        user_profile = database_fb.child("users").child(str(user[1]['localId'])).get()
        user_profile = dict(user_profile.val())
        user_profile['id'] = user[1]['localId']
        user_profile['email'] = user[1]['email']
        data.update({'base_user_profile':user_profile})
    else:
        data.update({'base_user_profile':None})
        print("data['base_user_profile']  = {}".format(data['base_user_profile']))
    return data


firebase = pyrebase.initialize_app(settings.FB_CONFIG)
auth_fb = firebase.auth()
database_fb = firebase.database()
storage_fb = firebase.storage()


def sign_in(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home:dashboard'))
    login_form = LoginForm(request.POST or None)
    next_url = request.GET.get('next_url')
    context = base(req=request)
    context['login_form'] = login_form
    context['next_url'] = next_url
    # return HttpResponse(next_url)
    if request.method == 'POST':
        if login_form.is_valid():
            clean_data = login_form.cleaned_data
            email = clean_data.get('username_or_email')
            password = clean_data.get('password')
            remember_me = clean_data.get('remember_me')

            # if remember_me:
            #     remember_me = True
            # else:
            #     remember_me = False
            #     remember_me = False
            try:
                user = auth_fb.sign_in_with_email_and_password(email=email,password=password)
                # return HttpResponse(next_url)
                if next_url == 'None' or not next_url:
                    next_url = reverse('home:dashboard')
                else:
                    pass
                # print('next_url={}')
                # # return HttpResponse(next_url)
                # print("user={}".format(user))
                session_id = user['idToken']
                request.session['uid'] = str(session_id)
                request.session.set_expiry(604800)

                return HttpResponseRedirect(next_url)
            except:
                message_login = _("Email or Password is incorrect")
            context['message_login'] = message_login

            # else:

    return render(request, 'home/general/sign-in.html', context=context)




def log_out(request):
    if check_user(req=request)[0]:
        auth.logout(request)
    else:
        auth.logout(request)
    return HttpResponseRedirect(reverse('home:sign_in'))



def dashboard(request):
    now = timezone.now()
    context = base_auth(req=request)

    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:dashboard'))

    # context['work_search_form'] = WorkSearchForm(request.POST or None, )
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        work_search_form = WorkSearchForm(request.POST or None,initial={'date':'{}'.format(now.strftime('%d-%m-%Y'))})
        context['work_search_form'] = work_search_form
    else:
        raise Http404
    return render(request, 'home/general/dashboard.html', context=context)



def general_all_user(request,u_slug):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:general-all-user', kwargs={'u_slug':u_slug}))

    if context['base_user_profile']['user_type'] == '1' or context['base_user_profile']['user_type'] == 1:
        pass
    else:
        raise Http404
    now = timezone.now()
    # own_user = database_fb.child("users").child(request.session['uid']).get()
    all_customers_list = []
    all_employees_list = []
    if u_slug == 'customers':
        all_users = database_fb.child("users").get()
    elif u_slug == 'employees':
        all_users = database_fb.child("users").get()
    else:
        raise Http404
    for all_users_item in all_users.each():
        # print(all_users_item.val()['user_type'])
        all_users_item_val = all_users_item.val()
        if int(all_users_item_val['user_type']) == 2:
            all_customers_list.append({
                'id':all_users_item.key(),
                'first_name':check_dict_key(all_users_item_val,'first_name'),
                'last_name':check_dict_key(all_users_item_val,'last_name'),
                'status':check_dict_key(all_users_item_val,'status'),
                'user_type':check_dict_key(all_users_item_val,'user_type'),
                'city':check_dict_key(all_users_item_val,'city'),
                'image':check_dict_key(all_users_item_val,'image'),
                'code':check_dict_key(all_users_item_val,'code'),
                'phone':check_dict_key(all_users_item_val,'phone'),
            })
        elif int(all_users_item.val()['user_type']) == 3:
            all_employees_list.append({
                'id':all_users_item.key(),
                'first_name':check_dict_key(all_users_item_val,'first_name'),
                'last_name':check_dict_key(all_users_item_val,'last_name'),
                'status':check_dict_key(all_users_item_val,'status'),
                'city':check_dict_key(all_users_item_val,'city'),
                'image':check_dict_key(all_users_item_val,'image'),
                'code':check_dict_key(all_users_item_val,'code'),
                'phone':check_dict_key(all_users_item_val,'phone'),
            })
    if u_slug == 'customers' and context['base_user_profile']['user_type'] != '2':
        all_data_list = all_customers_list
        template_name = 'home/admin/customer-list.html'
    elif u_slug == 'employees':
        if context['base_user_profile']['user_type'] == '2':
            pass
        all_data_list = all_employees_list
        template_name = 'home/general/employee-list.html'
    else:
        raise Http404
    # return HttpResponse(all_data_list)
    context['all_data_list'] = all_data_list
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, template_name=template_name, context=context)



def sign_up(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    import time
    from datetime import datetime
    import pytz

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404

    sign_up_form = SignupForm(request.POST or None, request.FILES or None)
    now = timezone.now()
    context['sign_up_form'] = sign_up_form
    # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if sign_up_form.is_valid():
            clean_data = sign_up_form.cleaned_data
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            email = clean_data.get('email')
            password = clean_data.get('password')
            code = clean_data.get('code')
            address = clean_data.get('address')
            post_code = clean_data.get('post_code')
            city = clean_data.get('city')
            phone = clean_data.get('phone')
            image = clean_data.get('image')
            document = clean_data.get('document')
            address_document = clean_data.get('address_document')
            contract_file = clean_data.get('contract_file')
            user_type = clean_data.get('user_type')
            try:
                user_fb = auth_fb.create_user_with_email_and_password(email=email,password=password)
            except:
                sign_up_form.add_error('email', _('Email allready use'))
                return render(request, 'home/admin/add-user.html', context=context)
            # idtoken = request.session['uid']
            # a = auth_fb.get_account_info(idtoken)
            # a = a['users']
            # a = a[0]
            # a = a['localId']
            # print("info"+str(a))
            # photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
            # document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
            # address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
            # contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
            # photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['uid'])
            # document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['uid'])
            # address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['uid'])
            # contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['uid'])
            # return HttpResponse(storage_fb.child(photo_file_name).get_url(request.session['uid']))
            # print("context['base_user_profile']={}".format(context['base_user_profile']))
            # print(photo_file_name_url)
            # return HttpResponse(photo_file_name_url)
            if code:
                pass
            else:
                code = ' '
            if address:
                pass
            else:
                address = ' '
            if post_code:
                pass
            else:
                post_code = ' '
            if city:
                pass
            else:
                city = ' '
            if phone:
                pass
            else:
                phone = ' '
            data = {
                'first_name':first_name,
                'last_name':last_name,
                'user_type':user_type,
                'status':1,
                'code':code,
                'address':address,
                'post_code':post_code,
                'city':city,
                # 'phone':phone,
                # 'image':storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens']),
                # 'document':storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens']),
                # 'address_document':storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens']),
                # 'contract_file':storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens']),
            }
            if image:
                photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
                photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['uid'])
                data['image'] = storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens'])
            else:
                data['image'] = ''
            if document:
                document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
                document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['uid'])
                data['document'] = storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens'])
            else:
                data['document'] = ''
            if address_document:
                address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
                address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['uid'])
                data['address_document'] = storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens'])
            else:
                data['address_document'] = ''
            if contract_file:
                contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
                contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['uid'])
                data['contract_file'] = storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens'])
            else:
                data['contract_file'] = ''
            database_fb.child("users").child(user_fb['localId']).set(data=data)
            success_message = 'Succesfully added'
            context['success_message'] = success_message
    # context['tours'] = Tour.objects.filter(active=True)
    sign_up_form = SignupForm()
    context['sign_up_form'] = sign_up_form
    return render(request, 'home/admin/add-user.html', context=context)






def edit_user(request,user_type,user_id):
    context = base_auth(req=request)

    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:edit-user', kwargs={'user_type':user_type,'user_id':user_id,}))


    if context['base_user_profile']['user_type'] == '1':
        pass
    else:
        raise Http404
    # print("mili"+str(millis))
    user = database_fb.child("users").child(user_id).get()
    user_detail = dict(user.val())
    # print("user_detail={}".format(user_detail))
    # print("check_dict_key(user_detail,'first_name')={}".format(check_dict_key(user_detail,'first_name')))
    sign_up_form = UserEditForm(request.POST or None, request.FILES or None,
                                       initial={
                                           'first_name': check_dict_key(user_detail,'first_name'),
                                           'last_name': check_dict_key(user_detail,'last_name'),
                                           'code': check_dict_key(user_detail,'code'),
                                           'address': check_dict_key(user_detail,'address'),
                                           'post_code': check_dict_key(user_detail,'post_code'),
                                           'city': check_dict_key(user_detail,'city'),
                                           'phone': check_dict_key(user_detail,'phone'),
                                           'user_type': check_dict_key(user_detail,'user_type'),
                                       },)
    now = timezone.now()
    context['sign_up_form'] = sign_up_form
    # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if sign_up_form.is_valid():
            clean_data = sign_up_form.cleaned_data
            first_name = clean_data.get('first_name')
            last_name = clean_data.get('last_name')
            email = clean_data.get('email')
            password = clean_data.get('password')
            code = clean_data.get('code')
            address = clean_data.get('address')
            post_code = clean_data.get('post_code')
            city = clean_data.get('city')
            phone = clean_data.get('phone')
            image = clean_data.get('image')
            document = clean_data.get('document')
            address_document = clean_data.get('address_document')
            contract_file = clean_data.get('contract_file')
            user_type = clean_data.get('user_type')

            # print(image)
            # return HttpResponse(sign_up_form.is_valid())
            # user_fb = auth_fb.create_user_with_email_and_password(email=email,password=password)
            # idtoken = request.session['uid']
            # a = auth_fb.get_account_info(idtoken)
            # a = a['users']
            # a = a[0]
            # a = a['localId']
            # print("info"+str(a))

            data = {
                'first_name':first_name,
                'last_name':last_name,
                'user_type':user_type,
                'status':1,
                'code':code,
                'address':address,
                'post_code':post_code,
                'city':city,
                'phone':phone,
            }
            if image:
                photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
                photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['uid'])
                data['image'] = storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens'])
            else:
                photo_file_name_url = check_dict_key(user_detail,'image')
                data['image'] = photo_file_name_url
            if document:
                document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
                document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['uid'])
                data['document'] = storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens'])
            else:
                document_file_name_url = check_dict_key(user_detail,'document')
                data['document'] = document_file_name_url
            if address_document:
                address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
                address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['uid'])
                data['address_document'] = storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens'])
            else:
                address_document_file_name_url = check_dict_key(user_detail,'address_document')
                data['address_document'] = address_document_file_name_url
            if contract_file:
                contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
                contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['uid'])
                data['contract_file'] = storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens'])
            else:
                contract_file_name_url = check_dict_key(user_detail,'contract_file')
                data['contract_file'] = contract_file_name_url
            # return HttpResponse(storage_fb.child(photo_file_name).get_url(request.session['uid']))
            # print("context['base_user_profile']={}".format(context['base_user_profile']))
            # print(photo_file_name_url)
            # return HttpResponse(photo_file_name_url)
            idtoken = request.session['uid']
            database_fb.child("users").child(user_id).update(data=data,token=idtoken)
            success_message = 'Succesfully Edited'
            context['success_message'] = success_message
            # sign_up_form = UserEditForm()
    # context['tours'] = Tour.objects.filter(active=True)
    context['sign_up_form'] = sign_up_form
    return render(request, 'home/admin/edit-user.html', context=context)




def employee_add_to_customer(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-add-to-customer'))

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404
    # return HttpResponse(check_user(req=request))
    # firebase = pyrebase.initialize_app(settings.FB_CONFIG)
    customer_employee_form = CustomerEmployeeForm(request.POST or None, request.FILES or None)
    now = timezone.now()
    # users = database_fb.child("users").child("users").order_by_child("first_name").get()                                                                                                                                                                            .get()
    success_message = ''
    # print(users)
    if request.method == 'POST':
        if customer_employee_form.is_valid():
            clean_data = customer_employee_form.cleaned_data
            customer = clean_data.get('customer')
            employees = clean_data.get('employees')
            start_date = clean_data.get('start_date')
            end_date = clean_data.get('end_date')
            data = {}
            for employees_item in employees:
                data[employees_item] = { 'admin_id': context['base_user_profile']['id'],'start_date': str(start_date.strftime("%d-%m-%Y %H:%M")), 'end_date': str(end_date.strftime("%d-%m-%Y %H:%M")) }
            database_fb.child("customer_employee").child(customer).child(str(start_date.strftime("%d-%m-%Y"))).set(data=data)
            success_message = 'Succesfully added'
            context['success_message'] = success_message
        # else:
        #     return HttpResponse(customer_employee_form.errors)
    customer_employee_form = CustomerEmployeeForm()
    context['customer_employee_form'] = customer_employee_form
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, 'home/admin/employee-add-to-customer.html', context=context)



from collections import OrderedDict


def employee_page(request,e_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee_page', kwargs={'e_id':e_id}))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404
    context['employee_id'] = e_id

    employee_work_search_form = EmployeePageWorkSearchForm(request.POST or None)
    context['employee_work_search_form'] = employee_work_search_form
    user = database_fb.child("users").child(e_id).get()
    # print("******************************************************************")
    context['user_details'] = dict(user.val())
    all_employees_list = []
    all_employees_dict = {}
    _html = ''
    customer_employees_dates_list = []

    # print("all_employees_dict={}".format(all_employees_dict))
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    return render(request, 'home/general/employee.html', context=context)



def employee_day_checks(request,e_id,day,month,year):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-day-checks', kwargs={'e_id':e_id,'day':day,'month':month,'year':year,}))
    try:
        if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
            pass
        else:
            raise Http404
    except:
        raise Http404
    context['employee_id'] = e_id
    checks_location_dayly = database_fb.child("checker_daily").child(e_id).child("{}-{}-{}".format(day,month,year)).get()
    checks_hourly_locations = database_fb.child("checker_hourly").child(e_id).child("{}-{}-{}".format(day,month,year)).get()
    user = database_fb.child("users").child(e_id).get()

    # return HttpResponse("user.val() = {}".format(dict(checks_location_dayly.val())))
    try:
        context['user_details'] = dict(user.val())
        context['checks_location_dayly'] = dict(checks_location_dayly.val())
        context['checks_location_dayly_keys'] = context['checks_location_dayly'].keys()
    except:
        raise Http404
    # print("checks_hourly_locations={}".format(checks_hourly_locations))
    # return HttpResponse(checks_hourly_locations)
    try:
        context['checks_hourly_locations'] = dict(checks_hourly_locations.val())
    except:
        context['checks_hourly_locations'] = {}
    # print(now.day)
    context['day'] = day
    context['month'] = month
    context['year'] = year
    return render(request, 'home/general/employee-day-checks.html', context=context)


def customer_works_list(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:customer-works-list'))
    if context['base_user_profile']['user_type'] == '2':
        pass
    else:
        raise Http404
    try:
        all_data_list_dict = {}
        customer_employee_dates = database_fb.child("customer_employee").child(context['base_user_profile']['id']).get()
        # customer_employee_dates = database_fb.child("customer_employee").child('lTp9xHfdA3WeWTqlprHF9oc5IY02').get()
        all_users = database_fb.child("users").get()
        all_users_list = []
        all_employees_list = []
        for all_users_item in all_users.each():
            all_users_item_val = all_users_item.val()
            if int(all_users_item.val()['user_type']) == 3:
                all_employees_list.append({
                    'id':all_users_item.key(),
                    'first_name':check_dict_key(all_users_item_val,'first_name'),
                    'last_name':check_dict_key(all_users_item_val,'last_name'),
                    'status':check_dict_key(all_users_item_val,'status'),
                    'city':check_dict_key(all_users_item_val,'city'),
                    'image':check_dict_key(all_users_item_val,'image'),
                    'code':check_dict_key(all_users_item_val,'code'),
                    'phone':check_dict_key(all_users_item_val,'phone'),
                })
        all_data_list = dict(customer_employee_dates.val())
        for all_data_list_item in all_data_list.items():
            # print(all_data_list_item[0])
            # print(all_data_list_item[1])
            for all_data_list_item_x in dict(all_data_list_item[1]).items():
                for all_employees_list_item in all_employees_list:
                    if all_employees_list_item['id'] == all_data_list_item_x[0]:
                        employee_f = all_employees_list_item
                        # print("all_data_list_item_x={}".format(all_data_list_item_x))
                        all_data_list_item_x[1]['employee_info'] = employee_f
                        # print("all_data_list_item_x={}".format(all_data_list_item_x))
                    else:
                        pass
        context['all_data_list'] = all_data_list
    except:
        context['all_data_list'] = None
    # print("context['customer_employee_dates']={}".format(context['customer_employee_dates']))
    # return HttpResponse('salam')
    return render(request, 'home/customer/my-employees.html', context=context)


def customer_page(request,c_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:customer-page', kwargs={'c_id':c_id}))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404
    context['customer_id'] = c_id
    user = database_fb.child("users").child(c_id).get()
    # print("******************************************************************")
    context['user_details'] = dict(user.val())
    try:
        all_data_list_dict = {}
        customer_employee_dates = database_fb.child("customer_employee").child(c_id).get()
        # customer_employee_dates = database_fb.child("customer_employee").child('lTp9xHfdA3WeWTqlprHF9oc5IY02').get()
        all_users = database_fb.child("users").get()
        all_users_list = []
        all_employees_list = []
        for all_users_item in all_users.each():
            all_users_item_val = all_users_item.val()
            if int(all_users_item.val()['user_type']) == 3:
                all_employees_list.append({
                    'id':all_users_item.key(),
                    'first_name':check_dict_key(all_users_item_val,'first_name'),
                    'last_name':check_dict_key(all_users_item_val,'last_name'),
                    'status':check_dict_key(all_users_item_val,'status'),
                    'city':check_dict_key(all_users_item_val,'city'),
                    'image':check_dict_key(all_users_item_val,'image'),
                    'code':check_dict_key(all_users_item_val,'code'),
                    'phone':check_dict_key(all_users_item_val,'phone'),
                })
        all_data_list = dict(customer_employee_dates.val())
        for all_data_list_item in all_data_list.items():
            # print(all_data_list_item[0])
            # print(all_data_list_item[1])
            for all_data_list_item_x in dict(all_data_list_item[1]).items():
                for all_employees_list_item in all_employees_list:
                    if all_employees_list_item['id'] == all_data_list_item_x[0]:
                        employee_f = all_employees_list_item
                        # print("all_data_list_item_x={}".format(all_data_list_item_x))
                        all_data_list_item_x[1]['employee_info'] = employee_f
                        # print("all_data_list_item_x={}".format(all_data_list_item_x))
                    else:
                        pass
        context['all_data_list'] = all_data_list
    except:
        context['all_data_list'] = None
    return render(request, 'home/admin/customer-page.html', context=context)




def employee_search(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # print("******************************************************************")
    # print(now.day)
    employee_work_search_form = EmployeeWorkSearchForm(context['base_user_profile']['user_type'],context['base_user_profile']['id'],request.POST or None)
    context['employee_work_search_form'] = employee_work_search_form
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            message_code = 1
            clean_data = employee_work_search_form.cleaned_data
            e_id = clean_data.get('employee', None)
            order_date_from = clean_data.get('start_date', None)
            order_date_to = clean_data.get('end_date', None)
            try:
                daily_checkers = database_fb.child("checker_daily").child(e_id).get()
                daily_checkers_val = daily_checkers.val()
            except:
                daily_checkers_val = None
            all_employees_list = []
            customer_employees_dates_list = []
            if daily_checkers_val:
                list_i = 0
                print("--------------------------------------------------------------------")
                print(daily_checkers_val.items())
                print("--------------------------------------------------------------------")
                for daily_checkers_val in daily_checkers_val.items():
                    daily_checkers_val_i += 1
                    # daily_checkers_val_date = daily_checkers_val[0]
                    append_bool = True
                    if int(context['base_user_profile']['user_type']) == 2:
                        try:
                            customer_employee_dates = database_fb.child("customer_employee").child(context['base_user_profile']['id']).get()
                            all_data_list = dict(customer_employee_dates.val())
                            for all_data_list_item in all_data_list.items():
                                # print(all_data_list_item[0])
                                # print(all_data_list_item[1])
                                for all_data_list_item_x in dict(all_data_list_item[1]).items():
                                    # print('hhhhhhhhhhhhhhhhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
                                    if [check_dict_key(all_data_list_item_x[1],'start_date'),check_dict_key(all_data_list_item_x[1],'end_date')] not in customer_employees_dates_list:
                                        # pass
                                        customer_employees_dates_list.append([check_dict_key(all_data_list_item_x[1],'start_date'),check_dict_key(all_data_list_item_x[1],'end_date')])
                                    # print("all_data_list_item_x[1]={}".format(all_data_list_item_x[1]))
                                    # print("all_data_list_item_x[0]={}".format(all_data_list_item_x[0]))
                                    # print('amiiiiiiiiiiiiiiiggggggggggggggggggoooooooooooooooooooooooooooooooooooooooooooooooooooo')
                            if len(customer_employees_dates_list)>0:
                                for customer_employees_dates_list_item in customer_employees_dates_list:
                                    pass
                            # print("customer_employees_dates_list={}".format(customer_employees_dates_list))
                        except:
                            pass
                        for customer_employees_dates_list_item in customer_employees_dates_list:
                            # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                            if customer_employees_dates_list_item[0]:
                                # print("customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                if datetime.strptime(str(customer_employees_dates_list_item[0]), '%d-%m-%Y %H:%M').date() > datetime.strptime(daily_checkers_val[0], '%d-%m-%Y').date():
                                    # print("1ci if odendi")
                                    append_bool = False
                                    continue
                                else:
                                    # print("1ci if odenmedi")
                                    append_bool = True
                            if append_bool and customer_employees_dates_list_item[1]:
                                # print("2ci customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                if append_bool and datetime.strptime(str(customer_employees_dates_list_item[1]),'%d-%m-%Y %H:%M').date() < datetime.strptime(
                                        daily_checkers_val[0], '%d-%m-%Y').date():
                                    # print("2ci if odendi")
                                    append_bool = False
                                    continue
                                else:
                                    pass
                                    # print("2ci if odenmedi")
                            if append_bool:
                                break
                            # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                    if order_date_from:
                        if datetime.strptime(str(order_date_from), '%d-%m-%Y').date() > datetime.strptime(daily_checkers_val[0], '%d-%m-%Y').date():
                            append_bool = False
                            continue
                    if append_bool and order_date_to:
                        if append_bool and datetime.strptime(str(order_date_to), '%d-%m-%Y').date() < datetime.strptime(daily_checkers_val[0], '%d-%m-%Y').date():
                            append_bool = False
                            continue
                    print("##########################################################################")
                    print("***************** daily_checkers_val {} = ".format(daily_checkers_val))
                    print("***************** daily_checkers_val[1] {} = ".format(daily_checkers_val[1]))
                    print("**************************************************************************")
                    if append_bool:
                        list_i += 1
                        for key_item in list(daily_checkers_val[1].keys()):
                            _html = '{0}{1}'.format(_html,render_to_string(
                                'home/include/_employee-work-row.html',
                                {
                                    'e_id': e_id,
                                    'list_i': list_i,
                                    'check_in_date':  check_dict_key(daily_checkers_val[1][key_item],'check_in_date'),
                                    'check_in_image_url': check_dict_key(daily_checkers_val[1][key_item],'check_in_image_url'),
                                    'check_out_date': check_dict_key(daily_checkers_val[1][key_item],'check_out_date'),
                                    'check_out_image_url': check_dict_key(daily_checkers_val[1][key_item],'check_out_image_url'),
                                    'date': daily_checkers_val[0],
                                    'url': reverse('home:employee-day-checks',
                                        kwargs={'e_id': e_id, 'day': str(daily_checkers_val[0]).split('-')[0],
                                                'month': str(daily_checkers_val[0]).split('-')[1],
                                                'year': str(daily_checkers_val[0]).split('-')[2], }),
                                }))

            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employee-work-table.html',
                {
                    'html': _html,
                }))
            # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,})
        else:
            pass
            # print('no valid')
            # print(employee_work_search_form.data)
            # print(employee_work_search_form.errors)
    return render(request, 'home/admin/employee-work-search.html', context=context)




def employee_page_search(request,e_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # print("******************************************************************")
    # print(now.day)
    employee_work_search_form = EmployeePageWorkSearchForm(request.POST or None)
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            message_code = 1
            clean_data = employee_work_search_form.cleaned_data
            order_date_from = clean_data.get('start_date', None)
            order_date_to = clean_data.get('end_date', None)
            try:
                daily_checkers = database_fb.child("checker_daily").child(e_id).get()
                daily_checkers_val = daily_checkers.val()
            except:
                daily_checkers_val = None
            all_employees_list = []
            customer_employees_dates_list = []
            if int(context['base_user_profile']['user_type']) == 2:
                try:
                    customer_employee_dates = database_fb.child("customer_employee").child(
                        context['base_user_profile']['id']).get()
                    all_data_list = dict(customer_employee_dates.val())
                    for all_data_list_item in all_data_list.items():
                        try:
                            all_data_list_item_x = dict(all_data_list_item[1])[e_id]
                            # print("try *********all_data_list_item_x = {}".format(all_data_list_item_x))
                            if [check_dict_key(all_data_list_item_x, 'start_date'),
                                check_dict_key(all_data_list_item_x,
                                               'end_date')] not in customer_employees_dates_list:
                                customer_employees_dates_list.append(
                                    [check_dict_key(all_data_list_item_x, 'start_date'),
                                     check_dict_key(all_data_list_item_x, 'end_date')])
                        except:
                            pass
                except:
                    pass
            if daily_checkers_val:
                list_i = 0
                for daily_checkers_val_item in daily_checkers_val.items():
                    # print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ = {}".format(daily_checkers_val))
                    daily_checkers_val_i += 1
                    # daily_checkers_val_date = daily_checkers_val[0]
                    append_bool = True
                    if int(context['base_user_profile']['user_type']) == 2:
                            # all_data_list_item_x = dict(all_data_list_item[1])[e_id]
                            # if [check_dict_key(all_data_list_item_x[1],'start_date'),check_dict_key(all_data_list_item_x[1],'end_date')] not in customer_employees_dates_list:
                            #     customer_employees_dates_list.append([check_dict_key(all_data_list_item_x[1],'start_date'),check_dict_key(all_data_list_item_x[1],'end_date')])
                        for customer_employees_dates_list_item in customer_employees_dates_list:
                            # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                            if customer_employees_dates_list_item[0]:
                                # print("customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                if datetime.strptime(str(customer_employees_dates_list_item[0]), '%d-%m-%Y %H:%M').date() > datetime.strptime(daily_checkers_val_item[0], '%d-%m-%Y').date():
                                    # print("1ci if odendi")
                                    append_bool = False
                                    continue
                                else:
                                    # print("1ci if odenmedi")
                                    append_bool = True
                            if append_bool and customer_employees_dates_list_item[1]:
                                # print("2ci customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                if append_bool and datetime.strptime(str(customer_employees_dates_list_item[1]),'%d-%m-%Y %H:%M').date() < datetime.strptime(
                                        daily_checkers_val_item[0], '%d-%m-%Y').date():
                                    # print("2ci if odendi")
                                    append_bool = False
                                    continue
                                else:
                                    pass
                                    # print("2ci if odenmedi")
                            if append_bool:
                                break
                            # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                    print(">>>>>>>>>>>>>>>>>>>>>>>>> append_bool = {} <<<<<<<<<<<<<<<<<<<<<<<".format(append_bool))
                    # print(datetime.strptime(str(order_date_from), '%d-%m-%Y').date())
                    # print(daily_checkers_val_item)
                    # print(datetime.strptime(daily_checkers_val_item[0], '%d-%m-%Y').date())
                    if order_date_from:
                        if datetime.strptime(str(order_date_from), '%d-%m-%Y').date() > datetime.strptime(daily_checkers_val_item[0], '%d-%m-%Y').date():
                            # append_bool = False
                            continue
                    if append_bool and order_date_to:
                        if append_bool and datetime.strptime(str(order_date_to), '%d-%m-%Y').date() < datetime.strptime(daily_checkers_val_item[0], '%d-%m-%Y').date():
                            # append_bool = False
                            continue
                    print("<<<<<<<<<<<<<<<<<<<<<<< append_bool = {} >>>>>>>>>>>>>>>>>>>>>>>>>".format(append_bool))
                    if append_bool:
                        list_i += 1
                        print("daily_checkers_val_item[1] = {}".format(daily_checkers_val_item[1]))
                        for key_item in list(daily_checkers_val_item[1].keys()):
                            _html = '{0}{1}'.format(_html,render_to_string(
                                'home/include/_employee-work-row.html',
                                {
                                    'e_id': e_id,
                                    'list_i': list_i,
                                    'check_in_date':  check_dict_key(daily_checkers_val_item[1][key_item],'check_in_date'),
                                    'check_in_image_url': check_dict_key(daily_checkers_val_item[1][key_item],'check_in_image_url'),
                                    'check_out_date': check_dict_key(daily_checkers_val_item[1][key_item],'check_out_date'),
                                    'check_out_image_url': check_dict_key(daily_checkers_val_item[1][key_item],'check_out_image_url'),
                                    'date': daily_checkers_val_item[0],
                                    'url': reverse('home:employee-day-checks',
                                        kwargs={'e_id': e_id, 'day': str(daily_checkers_val_item[0]).split('-')[0],
                                                'month': str(daily_checkers_val_item[0]).split('-')[1],
                                                'year': str(daily_checkers_val_item[0]).split('-')[2], }),
                                }))

            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employee-work-table.html',
                {
                    'html': _html,
                }))
            # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,})
        else:
            raise Http404()
            # print('no valid')
            # print(employee_work_search_form.data)
            # print(employee_work_search_form.errors)





def work_page_search(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # print("******************************************************************")
    # print(now.day)
    employee_work_search_form = WorkSearchForm(request.POST or None)
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            message_code = 1
            clean_data = employee_work_search_form.cleaned_data
            customer = clean_data.get('customer', None)
            date = clean_data.get('date', None)

            all_users = database_fb.child("users").get()
            all_employees_dict = {}
            for all_users_item in all_users.each():
                print(all_users_item.val()['user_type'])
                if int(all_users_item.val()['user_type']) == 3:
                    all_employees_dict[all_users_item.key()] = {"first_name":check_dict_key(all_users_item.val(), 'first_name'),"last_name":check_dict_key(all_users_item.val(), 'last_name')}
            try:
                checker_daily = database_fb.child("checker_daily").get()
                daily_checkers_each = checker_daily.each()
            except:
                daily_checkers_each = None
            all_employees_list = []
            customer_employees_dates_list = []
            if daily_checkers_each:
                if int(context['base_user_profile']['user_type']) == 2 or customer:
                    try:
                        if int(context['base_user_profile']['user_type']) == 2:
                            customer_employee_dates = database_fb.child("customer_employee").child(context['base_user_profile']['id']).get()
                        else:
                            customer_employee_dates = database_fb.child("customer_employee").child(customer).get()
                        all_data_list = dict(customer_employee_dates.val())
                        for all_data_list_item in all_data_list.items():
                            # print(all_data_list_item[0])
                            # print(all_data_list_item[1])
                            for all_data_list_item_x in dict(all_data_list_item[1]).items():
                                if [check_dict_key(all_data_list_item_x[1], 'start_date'),
                                    check_dict_key(all_data_list_item_x[1],
                                                   'end_date')] not in customer_employees_dates_list:
                                    customer_employees_dates_list.append(
                                        [check_dict_key(all_data_list_item_x[1], 'start_date'),
                                         check_dict_key(all_data_list_item_x[1], 'end_date')])
                    except:
                        pass
                list_i = 0
                if daily_checkers_each:
                    print("#$#$#$#$#$#$#$#$# = {}".format(daily_checkers_each))
                    print("----------------------------------------------------------------------")
                    print("----------------------------------------------------------------------")
                    for daily_checkers_each_item in daily_checkers_each:
                        # print(daily_checkers_each_item.key())  # Morty
                        e_id = daily_checkers_each_item.key()
                        daily_checkers_each_item_val = daily_checkers_each_item.val()  # {name": "Mortimer 'Morty' Smith"}
                        print(daily_checkers_each_item.val())  # {name": "Mortimer 'Morty' Smith"}
                        for daily_checkers_val in dict(daily_checkers_each_item_val).items():
                            daily_checkers_val_i += 1
                            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
                            print(daily_checkers_val)
                            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
                            # daily_checkers_val_date = daily_checkers_val[0]
                            append_bool = True
                            if int(context['base_user_profile']['user_type']) == 2:

                                for customer_employees_dates_list_item in customer_employees_dates_list:
                                    # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                                    if customer_employees_dates_list_item[0]:
                                        # print("customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                        if datetime.strptime(str(customer_employees_dates_list_item[0]), '%d-%m-%Y %H:%M').date() > datetime.strptime(daily_checkers_val[0], '%d-%m-%Y').date():
                                            # print("1ci if odendi")
                                            append_bool = False
                                            continue
                                        else:
                                            # print("1ci if odenmedi")
                                            append_bool = True
                                    if append_bool and customer_employees_dates_list_item[1]:
                                        # print("2ci customer_employees_dates_list_item[0]={}".format(customer_employees_dates_list_item[0]))
                                        if append_bool and datetime.strptime(str(customer_employees_dates_list_item[1]),'%d-%m-%Y %H:%M').date() < datetime.strptime(
                                                daily_checkers_val[0], '%d-%m-%Y').date():
                                            # print("2ci if odendi")
                                            append_bool = False
                                            continue
                                        else:
                                            pass
                                            # print("2ci if odenmedi")
                                    if append_bool:
                                        break
                                    # print("customer_employees_dates_list_item={}".format(customer_employees_dates_list_item))
                            if date:
                                pass
                            else:
                                print("date-x = {}".format(date))
                                print("date = '{}-{}-{}'.format(now.day,now.month,now.year)")
                                date = '{}-{}-{}'.format(now.day,now.month,now.year)
                            if append_bool and date:
                                if append_bool and datetime.strptime(str(date), '%d-%m-%Y').date() == datetime.strptime(daily_checkers_val[0], '%d-%m-%Y').date():
                                    append_bool = True
                                else:
                                    append_bool = False
                                    continue
                            if append_bool:
                                print("all_employees_dict={}".format(all_employees_dict))
                                list_i += 1
                                try:
                                    _html = '{0}{1}'.format(_html,render_to_string(
                                        'home/include/_employees-work-row.html',
                                        {
                                            'e_id': e_id,
                                            'list_i': list_i,
                                            'first_name':  all_employees_dict[e_id]['first_name'],
                                            'last_name':  all_employees_dict[e_id]['last_name'],
                                            'check_in_date':  check_dict_key(daily_checkers_val[1],'check_in_date'),
                                            'check_in_image_url': check_dict_key(daily_checkers_val[1],'check_in_image_url'),
                                            'check_out_date': check_dict_key(daily_checkers_val[1],'check_out_date'),
                                            'check_out_image_url': check_dict_key(daily_checkers_val[1],'check_out_image_url'),
                                            'date': daily_checkers_val[0],
                                            'url': reverse('home:employee-day-checks',
                                                kwargs={'e_id': e_id, 'day': str(daily_checkers_val[0]).split('-')[0],
                                                        'month': str(daily_checkers_val[0]).split('-')[1],
                                                        'year': str(daily_checkers_val[0]).split('-')[2], }),
                                        }))
                                except:
                                    pass

            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employees-work-table.html',
                {
                    'html': _html,
                }))
            # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,})
        else:
            pass
            # print('no valid')
            # print(employee_work_search_form.data)
            # print(employee_work_search_form.errors)


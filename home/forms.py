import os

from django import forms
from django.utils.translation import ugettext as _

import pyrebase
from django.conf import settings

firebase = pyrebase.initialize_app(settings.FB_CONFIG)
auth_fb = firebase.auth()
database_fb = firebase.database()

user_type_choice = (
    (1,'Admin'),
    (2,'Customer'),
    (3,'Employee'),
)

class LoginForm(forms.Form):
    username_or_email = forms.EmailField(max_length=255,min_length=2,required=True,widget=forms.EmailInput(attrs={'placeholder':_('Email'),'autocomplete':'off','class': 'form-control form-control-solid placeholder-no-fix',}))
    password = forms.CharField(max_length=255,required=True,widget=forms.PasswordInput(attrs={'placeholder':_('Password'),'autocomplete':'off','class': 'form-control form-control-solid placeholder-no-fix',}))
    remember_me = forms.BooleanField(required=False)

class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    last_name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    email = forms.EmailField(max_length=255,min_length=2,required=True,widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control',}))
    password = forms.CharField(max_length=255,required=True,widget=forms.PasswordInput(attrs={'autocomplete':'off','class': 'form-control',}))
    code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    post_code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    city = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    phone = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    image = forms.ImageField(required=False)
    document = forms.FileField(required=False)
    address_document = forms.FileField(required=False)
    contract_file = forms.FileField(required=False)
    user_type = forms.ChoiceField(required=True,choices=user_type_choice,widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control',}))


class UserEditForm(forms.Form):
    first_name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    last_name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    code = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    post_code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    city = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    phone = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    image = forms.ImageField(required=False)
    document = forms.FileField(required=False)
    address_document = forms.FileField(required=False)
    contract_file = forms.FileField(required=False)
    user_type = forms.ChoiceField(required=True,choices=user_type_choice,widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control',}))

class CustomerEmployeeForm(forms.Form):
    customer = forms.ChoiceField(required=True,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2',}))
    employees = forms.MultipleChoiceField(required=True,choices=[],widget=forms.SelectMultiple(attrs={'autocomplete':'off','class': 'form-control select2-multiple'}))
    start_date = forms.DateTimeField(required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':10,'readonly':'',}))
    end_date = forms.DateTimeField(required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,'readonly':'',}))
    def __init__(self,*args, **kwargs):
        super(CustomerEmployeeForm, self).__init__(*args, **kwargs)
        all_users = database_fb.child("users").get()
        all_customers_list = []
        all_employees_list = []
        for all_users_item in all_users.each():
            print(all_users_item.val()['user_type'])
            if int(all_users_item.val()['user_type']) == 2:
                all_customers_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])
            elif int(all_users_item.val()['user_type']) == 3:
                all_employees_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])
            else:
                pass
                # print("all_users_item.val() = {} ".format(all_users_item.val()))
        # customer_obj = customer_obj
        # print("all_employees_list = {}".format(all_employees_list))
        # employees_obj = customer_obj
        self.fields['customer'].choices = all_customers_list
        self.fields['employees'].choices = all_employees_list

class EmployeePageWorkSearchForm(forms.Form):
    start_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'From'}))
    end_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control','placeholder':'To',}))
    def __init__(self,*args, **kwargs):
        super(EmployeePageWorkSearchForm, self).__init__(*args, **kwargs)


class EmployeeWorkSearchForm(forms.Form):
    employee = forms.ChoiceField(required=True,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Employee'}))
    start_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'From'}))
    end_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control','placeholder':'To',}))
    def __init__(self,user_type,user_id,*args, **kwargs):
        super(EmployeeWorkSearchForm, self).__init__(*args, **kwargs)
        all_users = database_fb.child("users").get()
        all_employees_list = []
        customer_employees_list = []

        if int(user_type) == 2:
            try:
                customer_employee_dates = database_fb.child("customer_employee").child(user_id).get()
                all_data_list = dict(customer_employee_dates.val())
                for all_data_list_item in all_data_list.items():
                    # print(all_data_list_item[0])
                    # print(all_data_list_item[1])
                    for all_data_list_item_x in dict(all_data_list_item[1]).items():
                        # print('#############################################################################')
                        if all_data_list_item_x[0] not in customer_employees_list:
                            customer_employees_list.append(all_data_list_item_x[0])
                        # print(all_data_list_item_x)
                        # print('*****************************************************************************')
            except:
                pass
        # print("customer_employees_list={}".format(customer_employees_list))
        try:
            for all_users_item in all_users.each():
                print(all_users_item.val()['user_type'])
                if int(all_users_item.val()['user_type']) == 3:
                    if int(user_type) == 2:
                        if all_users_item.key() in customer_employees_list:
                            all_employees_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])
                    elif int(user_type) == 1:
                        all_employees_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])

                else:
                    pass
                    # print("all_users_item.val() = {} ".format(all_users_item.val()))
        except:
            pass
        # customer_obj = customer_obj
        # print("all_employees_list = {}".format(all_employees_list))
        # employees_obj = customer_obj
        self.fields['employee'].choices = all_employees_list



class WorkSearchForm(forms.Form):
    customer = forms.ChoiceField(required=False,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Customer',}))
    date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'Date'}))
    def __init__(self,*args, **kwargs):
        super(WorkSearchForm, self).__init__(*args, **kwargs)
        all_users = database_fb.child("users").get().each()
        all_customers_list = []
        # all_employees_list = []
        for all_users_item in all_users:
            print(all_users_item.val()['user_type'])
            if int(all_users_item.val()['user_type']) == 2:
                all_customers_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])
            # elif int(all_users_item.val()['user_type']) == 3:
            #     all_employees_list.append([all_users_item.key(),"{} {}".format(all_users_item.val()['first_name'],all_users_item.val()['last_name'])])
            else:
                pass
                # print("all_users_item.val() = {} ".format(all_users_item.val()))
        # customer_obj = customer_obj
        # print("all_employees_list = {}".format(all_employees_list))
        # employees_obj = customer_obj
        self.fields['customer'].choices = all_customers_list
        # self.fields['employees'].choices = all_employees_list
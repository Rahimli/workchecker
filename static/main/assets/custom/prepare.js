
        function preparePlan($id) {
            var plan_prepare_button = $('#'+$id);
            var url = plan_prepare_button.attr( "data-ajax-url" );
            $.ajax({
                dataType: "json",
                type: "GET",
                url: url,
                data: {},
                success: function (data) {
                    var message_code = data['message_code'];
                    var message = data['message'];
                    if (message_code == 1){
                        swal(
                          'Result',
                          message,
                          'success'
                        )
                    }
                    if (message_code == 0){
                        swal(
                          'Result',
                          message,
                          'info'
                        )
                    }
                    plan_prepare_button.attr('disabled','disabled');
                },
                error: function (data) {
                    plan_prepare_button.removeAttr('disabled');
                }
            });
        }

